package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"log"
	"os"
	"time"

	"github.com/chromedp/cdproto/io"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
)

func main() {
	log.Println("This is html2pdf, (C) 2020/24 by Martin Lercher")

	t0 := time.Now()
	defer func() {
		log.Println(time.Now().Sub(t0))
	}()

	render(
		`https://news.opensuse.org/`, "sample.pdf",
		`https://de.wikipedia.org/wiki/Wikipedia:Hauptseite`, "sample2.pdf",
	)
}

// code adapted from https://github.com/chromedp/examples/pull/26/commits/c281823d5024a3152a2914473b9be84142b9db33

func render(url0, fn0, url1, fn1 string) {
	// create context
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// capture pdf of a page
	var buf0, buf1 []byte
	err := chromedp.Run(ctx,
		printToPDF(url0, &buf0),
		printToPDF(url1, &buf1),
	)
	if err != nil {
		log.Fatal(err)
	}

	err = os.WriteFile(fn0, buf0, 0644)
	if err != nil {
		log.Fatal(err)
	}

	err = os.WriteFile(fn1, buf1, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

// printToPDF takes a pdf of a specific page.
func printToPDF(urlstr string, result *[]byte) chromedp.Tasks {
	var t0 time.Time
	return chromedp.Tasks{

		chromedp.ActionFunc(func(ctx context.Context) error {
			t0 = time.Now()
			return nil
		}),

		chromedp.Navigate(urlstr),

		chromedp.ActionFunc(func(ctx context.Context) error {
			log.Println("navigate", urlstr, time.Now().Sub(t0))
			return nil
		}),

		chromedp.ActionFunc(func(ctx context.Context) error {
			t0 := time.Now()
			defer func() {
				log.Println("render pdf", urlstr, time.Now().Sub(t0))
			}()

			// DIN A4	210 x 297 mm	8.3 x 11.7"
			// https://stackoverflow.com/questions/44575628/alter-the-default-header-footer-when-printing-to-pdf
			// date: formatted print date - title: document title - url: document location - pageNumber: current page number - totalPages: total pages in the document  For example, <span class=title></span> would generate span containing the title.
			pp := page.PrintToPDFParams{
				PaperWidth:          mm(210).inch(),
				PaperHeight:         mm(297).inch(),
				MarginTop:           mm(35).inch(),
				MarginBottom:        mm(35).inch(),
				MarginLeft:          mm(10).inch(),
				MarginRight:         mm(10).inch(),
				DisplayHeaderFooter: true,
				HeaderTemplate:      `<div style="border: 1px solid red; width: 100%;" class="text">h1<br/>h2<br/>h3<br/>h4<br/>h5<br/>h6</div>`,
				FooterTemplate:      `<div style="border: 1px solid red; width: 100%; transform: translate(0mm, 0mm);" class="text">footer <span style="margin-left: auto; display: inline-block;">page</span> <span class=pageNumber></span> of <span class=totalPages></span><br/>line 2<br/>line 3<br/>line 4<br/>line 5<br/>line 6</div>`,
				PreferCSSPageSize:   true,
				TransferMode:        page.PrintToPDFTransferModeReturnAsStream,
			}
			_, stream, err := pp.Do(ctx) // return as b64 is only valid if less than 4MB binaries, see https://superuser.com/questions/1472051/headless-chrome-hangs-when-trying-to-print-page-as-pdf
			if err != nil {
				return err
			}
			defer io.Close(stream)

			var buffer bytes.Buffer
			n := 0
			for {
				n++
				// read a chunk (1MiB) of data, which is enlarged by ~30%, b/c it is base64 encoded and then transferred
				data, eof, err := io.Read(stream).WithSize(1024 * 1024).Do(ctx)
				if err != nil {
					return err
				}
				// log.Println(n, eof, len(data))
				if eof {
					if len(data) != 0 {
						return errors.New("eof is true but data is not empty")
					}
					break
				}
				b, err := base64.StdEncoding.DecodeString(data)
				if err != nil {
					return err
				}
				_, err = buffer.Write(b)
				if err != nil {
					return err
				}
			}
			*result = buffer.Bytes()
			return nil
		}),
	}
}

type mm float64

func (m mm) inch() float64 {
	return float64(m) / 25.4
}
