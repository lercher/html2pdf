# html2pdf

Using Chrome's headless mode and dev API to render, or more precisely print html to pdf.

## Status

Incubating / Experimental

## Plan

Convert this experimental program to a micro service, accepting html and
streaming back pdf bytes containing a render of that html.

Typical whish-lists include:

* specifiy print-page sizes and margins
* customize headers and footers
* render multi-megabyte sized documents
* combine mutliple html chunks into one document
* tattoo page numbers and total page counts after combination
* omit headers/footer lines on first/title pages of a chunk
* omit page total counts for special prepended or trailing pages
* enable pictures and other async/dynamic ressources in headers and footers

## Ressources

* See <https://github.com/chromedp/chromedp>
* And this pull request <https://github.com/chromedp/examples/pull/26/commits/c281823d5024a3152a2914473b9be84142b9db33>

## Thoughts (cited from chromedp)

> I want to use chromedp on a headless environment

The simplest way is to run the Go program that uses chromedp inside the
[chromedp/headless-shell](https://hub.docker.com/r/chromedp/headless-shell/) image.
That image contains headless-shell, a smaller headless build of Chrome, which
chromedp is able to find out of the box.
